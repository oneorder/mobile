# oneorder app

This is the oneorder iOS and Android app written in React Native.

## Setup

### Requirements

You will need Node (including NPM) and Xcode. You'll need the `oneorder-api` repo running.

### Installation

```
npm install
```

### Running

```
npm run ios
OR
npm run android
```