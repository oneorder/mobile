import React, { Component } from 'react'
import { Provider } from 'react-redux'
import ApiClient from './src/redux/ApiClient'
import Navigation from './src/views/Navigation'

import create from './src/redux/create'
const client = new ApiClient()
const store = create(client)

export default class App extends Component {
  render () {
    return (
      <Provider store={store} helpers={{client}}>
        <Navigation />
      </Provider>
    )
  }
}
