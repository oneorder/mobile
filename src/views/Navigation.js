// import React, { Component } from 'react'
// import { View, Text, TouchableOpacity } from 'react-native'
// import PropTypes from 'prop-types'
import {StackNavigator } from 'react-navigation'

import Login from './Login'
import Order from './Order'

export default StackNavigator({
  Login: { screen: Login },
  Order: { screen: Order }
})
