import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { autobind } from 'core-decorators'
import { reduxForm, Field } from 'redux-form'
import { login } from '../redux/modules/user'

import { TextField } from './common/Fields'
import Button from './common/Button'

@connect(
  state => ({
    user: state.user.data
  }),
  dispatch => bindActionCreators({
    login
  }, dispatch)
)
@reduxForm({
  form: 'login'
})
export default class Login extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    login: PropTypes.func,
    navigation: PropTypes.object
  }

  static navigationOptions = {
    title: 'Login'
  }

  componentDidMount () {
    const { navigate } = this.props.navigation

    AsyncStorage.getItem('@user:authToken', (err, result) => {
      if (result) {
        navigate('Order')
      }
    })
  }

  @autobind
  async loginHandler (credentials) {
    const { navigation: { navigate } } = this.props

    await this.props.login(credentials)

    navigate('Order')
  }

  render () {
    const { handleSubmit } = this.props

    return (
      <View
        style={{
          alignItems: 'center',
          flexGrow: 1
        }}
      >
        <Text
          style={{
            fontFamily: 'Futura-CondensedExtraBold',
            fontSize: 48
          }}
        >
          ONEORDER
        </Text>
        <Field
          name="email"
          placeholder="LOGIN"
          component={TextField}
          icon={() => (
            <Image source={require('../images/icPersonOutline.png')} />
          )}
        />
        <Field
          name="password"
          placeholder="PASSWORD"
          type="password"
          component={TextField}
          icon={() => (
            <Image source={require('../images/icLockOutline.png')} />
          )}
        />
        <Button
          icon={require('../images/icGrain.png')}
          onPress={handleSubmit(this.loginHandler)}
        >
          SIGN IN
        </Button>
      </View>
    )
  }
}
