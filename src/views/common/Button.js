import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text, Image } from 'react-native'

const Button = ({ children, icon, onPress }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        backgroundColor: '#333',
        height: 46,
        width: 221,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 17
      }}
    >
      {icon &&
        <Image
          source={icon}
        />
      }
      <Text
        style={{
          flexGrow: 1,
          color: '#FFF',
          textAlign: 'center'
        }}
      >
        {children}
      </Text>
    </TouchableOpacity>
  )
}

Button.propTypes = {
  children: PropTypes.string,
  icon: PropTypes.number,
  onPress: PropTypes.func
}

export default Button
