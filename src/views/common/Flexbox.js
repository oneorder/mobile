import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'

const Flexbox = ({ onPress, children, style, direction }) => {
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: direction,
        ...style
      }}
      onPress={onPress}
    >
      {children}
    </View>
  )
}

const Column = (props) => {
  return (
    <Flexbox
      {...props}
      direction="column"
    />
  )
}

const Row = (props) => {
  return (
    <Flexbox
      {...props}
      direction="row"
    />
  )
}

const commonProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
    PropTypes.array
  ]),
  style: PropTypes.object
}

Row.propTypes = commonProps
Column.propTypes = commonProps
Flexbox.propTypes = {
  direction: PropTypes.string,
  ...commonProps
}

export { Row, Column }
