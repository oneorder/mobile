import React from 'react'
import { TextInput, View, Text } from 'react-native'
import PropTypes from 'prop-types'
// import { Field } from 'redux-form'
// import { capitalize } from 'lodash'

// import classes from './Fields.scss'
// import Button from '../Button'
import { Column } from './Flexbox'

// const Checkbox = ({ children, input, meta: { touched, error } }) => {
//   const showError = touched && error;

//   return (
//     <div>
//       <label
//         className={input.value ? classes.labelChecked : classes.label}
//         htmlFor={`Checkbox_${input.name}`}
//       >
//         <input
//           {...input}
//           id={`Checkbox_${input.name}`}
//           className={input.value ? classes.checkboxChecked : classes.checkbox}
//           type="checkbox"
//         />
//         <span className={classes.text}>{children}</span>
//       </label>
//       {showError &&
//         <span className={classes.error}>{error}</span>
//       }
//     </div>
//   );
// };

// const Radio = ({ label, className, style, checkedClassName, options, input, meta: { touched, error } }) => {
//   const showError = touched && error

//   return (
//     <div>
//       <label>{label}</label>
//       <div className={classes.radioContainer} style={style}>
//         {options.map((option, index) => {
//           const radioId = `Radio_${option}`
//           const isChecked = input.value === option
//           let labelClass = className

//           if (!className && !checkedClassName) {
//             labelClass = isChecked ? classes.labelChecked : classes.label
//           }

//           return (
//             <label
//               key={index}
//               htmlFor={radioId}
//               className={labelClass}
//             >
//               <input
//                 {...input}
//                 id={radioId}
//                 type="radio"
//                 value={option}
//                 name={input.name}
//                 className={isChecked ? classes.checkboxChecked : classes.checkbox}
//               />
//               <span className={classes.text}>{capitalize(option)}</span>
//             </label>
//           )
//         })}
//       </div>
//       {showError &&
//         <span className={classes.error}>{error}</span>
//       }
//     </div>
//   )
// };

const TextField = ({ label, type, input: { onChange, ...input }, meta: { touched, error }, ...props }) => {
  const showError = touched && error;
  const keyboardType = (!type || type === 'password') ? 'default' : type

  return (
    <Column>
      <Text>
        {label}
      </Text>
      <View
        style={{
          flexDirection: 'row',
          height: 46,
          width: 221,
          borderWidth: 2,
          borderStyle: 'solid',
          borderColor: '#333',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: 0,
          paddingTop: 8,
          paddingRight: 12,
          paddingBottom: 8,
          paddingLeft: 12,
          ...props.style
        }}
      >
        {props.icon &&
          <props.icon />
        }
        <TextInput
          onChangeText={onChange}
          keyboardType={keyboardType}
          secureTextEntry={(type === 'password')}
          placeholder={props.placeholder}
          style={{
            flexGrow: 1,
            textAlign: 'center'
          }}
          {...input}
        />
      </View>
      {showError &&
        <Text>{error}</Text>
      }
    </Column>
  )
}

// const Textarea = ({ label, input, meta: { touched, error } }) => {
//   const showError = touched && error;
//   const fieldId = `Textarea_${input.name}`

//   return (
//     <div className={classes.textFieldContainer}>
//       <label
//         className={classes.text}
//         htmlFor={fieldId}
//       >
//         {label}
//       </label>
//       <textarea
//         {...input}
//         id={fieldId}
//       >
//         {input.value}
//       </textarea>
//       {showError &&
//         <span className={classes.error}>{error}</span>
//       }
//     </div>
//   )
// }

// const TextFieldArray = ({ fields, buttonLabel, label }) => {
//   return (
//     <div>
//       <label htmlFor={fields.name}>
//         {label}
//       </label>
//       {fields.map((field, index) => {
//         return (
//           <Field
//             name={field}
//             key={index}
//             component={TextField}
//           />
//         )
//       })}
//       <Button
//         onClick={() => fields.push()}
//         noSubmit
//         small
//       >
//         {buttonLabel}
//       </Button>
//     </div>
//   )
// }

// const Select = ({ style, options, label, input, error, touched, capitalize: caps }) => {
//   const showError = touched && error

//   return (
//     <Column
//       style={style}
//     >
//       <label htmlFor={input.name}>
//         {label}
//       </label>
//       <select
//         {...input}
//         id={input.name}
//       >
//         {options.map((option, index) => {
//           return (
//             <option key={index} value={option}>
//               {caps ? capitalize(option) : option}
//             </option>
//           )
//         })}
//       </select>
//       {showError &&
//         <span className={classes.error}>{error}</span>
//       }
//     </Column>
//   )
// }

const commonProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]),
  input: PropTypes.object,
  meta: PropTypes.object,
  style: PropTypes.object,
  placeholder: PropTypes.string
}

// Checkbox.propTypes = commonProps
TextField.propTypes = {
  label: PropTypes.string,
  ...commonProps
}
// Radio.propTypes = {
//   ...commonProps,
//   options: PropTypes.array
// }
// Radio.propTypes = {
//   ...commonProps,
//   checkedClassName: PropTypes.string
// }
// Select.propTypes = {
//   ...commonProps,
//   options: PropTypes.array
// }

export { TextField }
