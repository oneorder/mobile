import {createStore, applyMiddleware} from 'redux'
import createMiddleware from './clientMiddleware'
import rootReducer from './modules/reducer'

export default function configureStore (client) {
  const middleware = [createMiddleware(client)]
  const finalCreateStore = applyMiddleware(...middleware)(createStore)
  const store = finalCreateStore(rootReducer)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./modules/reducer').default
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}
