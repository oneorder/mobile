import { AsyncStorage } from 'react-native'

const LOGIN = 'events/LOGIN'
const LOGIN_SUCCESS = 'events/LOGIN_SUCCESS'
const LOGIN_FAILURE = 'events/LOGIN_FAILURE'

const initialState = {
  data: null,
  loaded: false
}

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      AsyncStorage.setItem('@user:authToken', action.result.data.authToken)
      return {
        ...state,
        ...action.result,
        loaded: true
      }

    default:
      return state
  }
}

export function login (credentials) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE],
    promise: (client) => client.post('/session', {
      data: credentials
    })
  }
}
