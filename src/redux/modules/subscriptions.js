const POST_SUBSCRIPTION = 'redux/modules/POST_SUBSCRIPTION'
const POST_SUBSCRIPTION_SUCCESS = 'redux/modules/POST_SUBSCRIPTION_SUCCESS'
const POST_SUBSCRIPTION_FAILURE = 'redux/modules/POST_SUBSCRIPTION_FAILURE'

const initialValues = {
  deviceId: "",
  loading: false,
  loaded: false
}

export default function reducer (state = initialValues, action = {}) {
  switch (action.type) {
    case POST_SUBSCRIPTION:
      console.log('posting subscription')
      return {
        ...initialValues,
        loading: true
      }

    case POST_SUBSCRIPTION_SUCCESS:
      console.log("success", action.result)
      return {
        ...state,
        ...action.result,
        loading: false,
        loaded: true
      }

    case POST_SUBSCRIPTION_FAILURE:
      return {
        ...state,
        ...action.result,
        loading: false,
        loaded: false
      }

    default:
      return state
  }
}

export function postSubscription (deviceId) {
  return {
    types: [POST_SUBSCRIPTION, POST_SUBSCRIPTION_SUCCESS, POST_SUBSCRIPTION_FAILURE],
    promise: (client) => client.post('/subscription', {
      data: { deviceId }
    })
  }
}
