require('babel-polyfill');

module.exports = {
  apiHost: 'http://localhost:4000/v1',
  app: {
    title: 'ONEORDER App',
    description: 'All the modern best practices in one example.'
  }
};
